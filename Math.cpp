//
//  Math.cpp
//  Digital_InOut
//
//  Created by K-Taro on 2016/04/24.
//  Copyright © 2016年 K-Taro. All rights reserved.
//

#include "Math.hpp"


int Math::map(int x, int in_min, int in_max, int out_min, int out_max){
    return ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

float Math::map_f(float x, float in_min, float in_max, float out_min, float out_max){
    return ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

int Math::constrain(int x, int min, int max){
    if(x<min){
        return min;
    }else{
        if(x>max){
            return max;
        }else{
            return x;
        }
    }
}

float Math::constrain_f(float x, float min, float max){
    if(x<min){
        return min;
    }else{
        if(x>max){
            return max;
        }else{
            return x;
        }
    }
}

float Math::abs_f(float data){
    if(data < 0){
        return (-1)*data;
    }else{
        return data;
    }
}

int Math::round(float data){
    return (int)(data+0.5f);
}
