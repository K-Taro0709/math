//
//  Math.hpp
//  Digital_InOut
//
//  Created by K-Taro on 2016/04/24.
//  Copyright © 2016年 K-Taro. All rights reserved.
//

#ifndef Math_hpp
#define Math_hpp

#include <stdio.h>


class Math{
private:
    
public:
    int map(int x, int in_min, int in_max, int out_min, int out_max);
    float map_f(float x, float in_min, float in_max, float out_min, float out_max);
    int constrain(int x, int min, int max);
    float constrain_f(float x, float min, float max);
    float abs_f(float data);
    int round(float data);
    
};


#endif /* Math_hpp */
